# coding=utf-8

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox(executable_path=r'C:\EDGAR-PC\ANDES-2017-1\PADS\TrabajoSemana9\geckodriver.exe')
        self.browser.implicitly_wait(2)

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get("http://localhost:8000")
        self.assertIn("Busco Ayuda", self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()
        time.sleep(3)
        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Edgar Yesid')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Camacho')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('10')

        self.browser.find_element_by_xpath(
            "//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3114589043')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('ey.camacho10@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_imagen')


        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('ey.camacho10')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        time.sleep(3)
        span = self.browser.find_element(By.XPATH, "//span[text()='Edgar Yesid Camacho']")

        self.assertIn('Edgar Yesid Camacho', span.text)


    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span=self.browser.find_element(By.XPATH, "//span[text()='Edgar Yesid Camacho']")
        span.click()

        h2=self.browser.find_element(By.XPATH, "//h2[text()='Edgar Yesid Camacho']")

        self.assertIn('Edgar Yesid Camacho', h2.text)